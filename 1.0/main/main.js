function return_array_of_hostnames(ns) {
    let array_of_hostnames = [ns.getHostname()];
	var i;
    for (i = 0; i < array_of_hostnames.length; i++) {
        let array_of_scan_results = ns.scan(array_of_hostnames[i]);
		var j;
        for (j = 0; j < array_of_scan_results.length; j++) {
            if (array_of_hostnames.indexOf(array_of_scan_results[j]) === -1) {
                array_of_hostnames.push(array_of_scan_results[j]);
            }
        }
    }
    return array_of_hostnames;
}

function return_array_of_rooted_hostnames(ns) {
    let array_of_hostnames = return_array_of_hostnames(ns);
	let array_of_rooted_hostnames = [];
	var i;
    for (i = 0; i < array_of_hostnames.length; i++) {
		if (ns.hasRootAccess(array_of_hostnames[i])) {
			array_of_rooted_hostnames.push(array_of_hostnames[i]);
		}
    }
    return array_of_rooted_hostnames;
}

export async function main(ns) {
	let host = ns.getHostname();
	let target = ns.args[0]
	let files = ["weaken.js", "grow.js", "hack.js"];
	let array_of_rooted_hosts = return_array_of_rooted_hostnames(ns);

	while (true) {
		var i;
		for (i = 0; i < array_of_rooted_hosts.length; i++) {
			let res = ns.getServerRam(array_of_rooted_hosts[i]);
			let ram_free = res[0] - res[1];
			let threads_weaken = Math.trunc(ram_free / ns.getScriptRam(files[0]));
			let threads_grow = Math.trunc(ram_free / ns.getScriptRam(files[1]));
			let threads_hack = Math.trunc(ram_free / ns.getScriptRam(files[2]));

			ns.scp(files, host, array_of_rooted_hosts[i]);
			if ((ns.getServerSecurityLevel(target) >= ns.getServerMinSecurityLevel(target) * 1.01) && (threads_weaken > 0)) {
				await ns.exec(files[0], array_of_rooted_hosts[i], threads_weaken, target);
			}
			else if ((ns.getServerMoneyAvailable(target) <= ns.getServerMaxMoney(target) * 0.99) && (threads_grow > 0)) {
				await ns.exec(files[1], array_of_rooted_hosts[i], threads_grow, target);
			}
			else if (threads_hack > 0) {
				await ns.exec(files[2], array_of_rooted_hosts[i], threads_hack, target);
			}
		}
		await ns.sleep(1);
	}
}