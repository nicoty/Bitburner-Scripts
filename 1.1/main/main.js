function return_number_of_exploits(ns) {
	let number_of_exploits = 0;
	let array_of_exploits = ['BruteSSH.exe', 'FTPCrack.exe', 'relaySMTP.exe', 'HTTPWorm.exe', 'SQLInject.exe'];
	var i;
	for (i = 0; i < array_of_exploits.length; i++) {
		if (ns.fileExists(array_of_exploits[i], ns.getHostname())) {
			++number_of_exploits;
		}
	}
	return number_of_exploits;
}

function return_array_of_hostnames(ns) {
    let array_of_hostnames = [ns.getHostname()];
	var i;
    for (i = 0; i < array_of_hostnames.length; i++) {
        let array_of_scan_results = ns.scan(array_of_hostnames[i]);
		var j;
        for (j = 0; j < array_of_scan_results.length; j++) {
            if (array_of_hostnames.indexOf(array_of_scan_results[j]) === -1) {
                array_of_hostnames.push(array_of_scan_results[j]);
            }
        }
    }
    return array_of_hostnames;
}

function return_array_of_rooted_hostnames(ns) {
    let array_of_hostnames = return_array_of_hostnames(ns);
	let array_of_rooted_hostnames = [];
	var i;
    for (i = 0; i < array_of_hostnames.length; i++) {
		if (ns.hasRootAccess(array_of_hostnames[i])) {
			array_of_rooted_hostnames.push(array_of_hostnames[i]);
		}
    }
    return array_of_rooted_hostnames;
}

function return_array_of_unrooted_hostnames(ns) {
    let array_of_hostnames = return_array_of_hostnames(ns);
	let array_of_unrooted_hostnames = [];
	var i;
    for (i = 0; i < array_of_hostnames.length; i++) {
		if (!ns.hasRootAccess(array_of_hostnames[i])) {
			array_of_unrooted_hostnames.push(array_of_hostnames[i]);
		}
    }
    return array_of_unrooted_hostnames;
}

export async function main(ns) {
	let host = ns.getHostname();
	let target = ns.args[0]
	let scripts = ["weaken.js", "grow.js", "hack.js"];
	
	while (true) {
		let array_of_rooted_hostnames = return_array_of_rooted_hostnames(ns);
		let array_of_unrooted_hostnames = return_array_of_unrooted_hostnames(ns);
		let number_of_exploits = return_number_of_exploits(ns);
		let hacking_level = ns.getHackingLevel();
		
		var h;
		for (h = 0; h < array_of_unrooted_hostnames.length; h++) {
			let unrooted_hostname = array_of_unrooted_hostnames[h];
			let number_of_ports_required = ns.getServerNumPortsRequired(unrooted_hostname);
			
			if (
					(number_of_ports_required <= number_of_exploits) && 
					(ns.getServerRequiredHackingLevel(unrooted_hostname) <= hacking_level)
					) {
						if (number_of_ports_required > 4) {
							ns.sqlinject(unrooted_hostname);
							ns.httpworm(unrooted_hostname);
							ns.relaysmtp(unrooted_hostname);
							ns.ftpcrack(unrooted_hostname);
							ns.brutessh(unrooted_hostname);
						}
						else if (number_of_ports_required > 3) {
							ns.httpworm(unrooted_hostname);
							ns.relaysmtp(unrooted_hostname);
							ns.ftpcrack(unrooted_hostname);
							ns.brutessh(unrooted_hostname);
						}
						else if (number_of_ports_required > 2) {
							ns.relaysmtp(unrooted_hostname);
							ns.ftpcrack(unrooted_hostname);
							ns.brutessh(unrooted_hostname);
						}
						else if (number_of_ports_required > 1) {
							ns.ftpcrack(unrooted_hostname);
							ns.brutessh(unrooted_hostname);
						}
						else if (number_of_ports_required > 0) {
							ns.brutessh(unrooted_hostname);
						}
						ns.nuke(unrooted_hostname);
			}
		}
		
		var i;
		for (i = 0; i < array_of_rooted_hostnames.length; i++) {
			let rooted_hostname = array_of_rooted_hostnames[i];
			let res = ns.getServerRam(rooted_hostname);
			let ram_free = res[0] - res[1];
			let threads_weaken = Math.trunc(ram_free / ns.getScriptRam(scripts[0]));
			let threads_grow = Math.trunc(ram_free / ns.getScriptRam(scripts[1]));
			let threads_hack = Math.trunc(ram_free / ns.getScriptRam(scripts[2]));

			ns.scp(scripts, host, rooted_hostname);
			if ((ns.getServerSecurityLevel(target) >= ns.getServerMinSecurityLevel(target) * 1.01) && (threads_weaken > 0)) {
				await ns.exec(scripts[0], rooted_hostname, threads_weaken, target);
			}
			else if ((ns.getServerMoneyAvailable(target) <= ns.getServerMaxMoney(target) * 0.99) && (threads_grow > 0)) {
				await ns.exec(scripts[1], rooted_hostname, threads_grow, target);
			}
			else if (threads_hack > 0) {
				await ns.exec(scripts[2], rooted_hostname, threads_hack, target);
			}
		}
		await ns.sleep(1);
	}
}