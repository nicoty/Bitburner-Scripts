function return_number_of_exploits(ns) {
	let number_of_exploits = 0;
	let array_of_exploits = ['BruteSSH.exe', 'FTPCrack.exe', 'relaySMTP.exe', 'HTTPWorm.exe', 'SQLInject.exe'];
	var i;
	for (i = 0; i < array_of_exploits.length; i++) {
		if (ns.fileExists(array_of_exploits[i], ns.getHostname())) {
			++number_of_exploits;
		}
	}
	return number_of_exploits;
}

function return_array_of_hostnames(ns) {
    let array_of_hostnames = [ns.getHostname()];
	var i;
    for (i = 0; i < array_of_hostnames.length; i++) {
        let array_of_scan_results = ns.scan(array_of_hostnames[i]);
		var j;
        for (j = 0; j < array_of_scan_results.length; j++) {
            if (array_of_hostnames.indexOf(array_of_scan_results[j]) === -1) {
                array_of_hostnames.push(array_of_scan_results[j]);
            }
        }
    }
    return array_of_hostnames;
}

function return_array_of_rooted_hostnames(ns) {
    let array_of_hostnames = return_array_of_hostnames(ns);
	let array_of_rooted_hostnames = [];
	var i;
    for (i = 0; i < array_of_hostnames.length; i++) {
		if (ns.hasRootAccess(array_of_hostnames[i])) {
			array_of_rooted_hostnames.push(array_of_hostnames[i]);
		}
    }
    return array_of_rooted_hostnames;
}

function return_total_free_ram_from_all_rooted_hostnames(ns) {
    let array_of_rooted_hostnames = return_array_of_rooted_hostnames(ns);
	var free_ram_from_all_rooted_hostnames = 0.0;
	var i;
	for (i = 0; i < array_of_rooted_hostnames.length; i++) {
		let res = ns.getServerRam(array_of_rooted_hostnames[i]);
		let ram_free = res[0] - res[1];
		free_ram_from_all_rooted_hostnames += ram_free;
	}
	return free_ram_from_all_rooted_hostnames;
}

function return_array_of_unrooted_hostnames(ns) {
    let array_of_hostnames = return_array_of_hostnames(ns);
	let array_of_unrooted_hostnames = [];
	var i;
    for (i = 0; i < array_of_hostnames.length; i++) {
		if (!ns.hasRootAccess(array_of_hostnames[i])) {
			array_of_unrooted_hostnames.push(array_of_hostnames[i]);
		}
    }
    return array_of_unrooted_hostnames;
}

function return_percentage_of_cash_from_available_per_hack(ns, server) { // adapted from scriptCalculatePercentMoneyHacked() in https://github.com/danielyxie/bitburner/blob/master/src/NetscriptEvaluator.js
    var difficultyMult = (100 - ns.getServerSecurityLevel(server)) / 100;
    var skillMult = (ns.getHackingLevel() - (ns.getServerRequiredHackingLevel(server) - 1)) / ns.getHackingLevel();
    var percentMoneyHacked = difficultyMult * skillMult * ns.getHackingMultipliers().money / 240;
    if (percentMoneyHacked < 0) {return 0;}
    if (percentMoneyHacked > 1) {return 1;}
    return percentMoneyHacked;
}

export async function main(ns) {
	let host = ns.getHostname();
	let target = ns.args[0]
	let scripts = ["weaken.js", "grow.js", "hack.js", "hack_delayed.js"];
	let percentage_of_cash_to_get = ns.args[1];
	
	while (true) {
		let array_of_rooted_hostnames = return_array_of_rooted_hostnames(ns);
		let array_of_unrooted_hostnames = return_array_of_unrooted_hostnames(ns);
		let number_of_exploits = return_number_of_exploits(ns);
		let hacking_level = ns.getHackingLevel();
		
		// crawls, nukes
		var h;
		for (h = 0; h < array_of_unrooted_hostnames.length; h++) {
			let unrooted_hostname = array_of_unrooted_hostnames[h];
			let number_of_ports_required = ns.getServerNumPortsRequired(unrooted_hostname);
			
			if (
					(number_of_ports_required <= number_of_exploits) && 
					(ns.getServerRequiredHackingLevel(unrooted_hostname) <= hacking_level)
					) {
						if (number_of_ports_required > 4) {
							ns.sqlinject(unrooted_hostname);
							ns.httpworm(unrooted_hostname);
							ns.relaysmtp(unrooted_hostname);
							ns.ftpcrack(unrooted_hostname);
							ns.brutessh(unrooted_hostname);
						}
						else if (number_of_ports_required > 3) {
							ns.httpworm(unrooted_hostname);
							ns.relaysmtp(unrooted_hostname);
							ns.ftpcrack(unrooted_hostname);
							ns.brutessh(unrooted_hostname);
						}
						else if (number_of_ports_required > 2) {
							ns.relaysmtp(unrooted_hostname);
							ns.ftpcrack(unrooted_hostname);
							ns.brutessh(unrooted_hostname);
						}
						else if (number_of_ports_required > 1) {
							ns.ftpcrack(unrooted_hostname);
							ns.brutessh(unrooted_hostname);
						}
						else if (number_of_ports_required > 0) {
							ns.brutessh(unrooted_hostname);
						}
						ns.nuke(unrooted_hostname);
			}
		}

		// weakens, grows, hacks
		var i;
		for (i = 0; i < array_of_rooted_hostnames.length; i++) {
			let rooted_hostname = array_of_rooted_hostnames[i];
			let res = ns.getServerRam(rooted_hostname);
			if (res[0] > 1) {
				let ram_free = res[0] - res[1];
				let total_ram_free = return_total_free_ram_from_all_rooted_hostnames(ns);
				let threads_weaken = Math.floor(ram_free / ns.getScriptRam(scripts[0]));
				let threads_grow = Math.floor(ram_free / ns.getScriptRam(scripts[1]));
				let threads_hack_max = Math.floor(ram_free / ns.getScriptRam(scripts[3])); // max hack
				var threads_hack_percentage = Math.floor((Math.pow(threads_hack_max, 2) * percentage_of_cash_to_get) / (return_percentage_of_cash_from_available_per_hack(ns, target) * Math.pow(Math.floor(total_ram_free / ns.getScriptRam(scripts[3])), 2))); // percentage hack, see notes.txt for how formula was derived
				if (threads_hack_percentage < 1) {
					threads_hack_percentage = 1;
				}
				var threads_hack = 0;
				if ((threads_hack_percentage) > (threads_hack_max)) {
					threads_hack = threads_hack_max;
				}
				else {
					threads_hack = threads_hack_percentage;
				}

				ns.scp(scripts, host, rooted_hostname);
				if (
					(ns.getServerSecurityLevel(target) > ns.getServerMinSecurityLevel(target)) &&
					(threads_weaken > 0) &&
					!(ns.isRunning(scripts[0], rooted_hostname))
				) {
					await ns.exec(scripts[0], rooted_hostname, threads_weaken, target);
				}
				if (
					(ns.getServerMoneyAvailable(target) >= ns.getServerMaxMoney(target)) &&
					(ns.getServerSecurityLevel(target) <= ns.getServerMinSecurityLevel(target) + (0.05 * Math.floor(total_ram_free / ns.getScriptRam(scripts[0])))) && // 0.05 is the amount weaken() decreases security by
					(threads_hack > 0) && 
					!(ns.isRunning(scripts[1], rooted_hostname)) &&
					!(ns.isRunning(scripts[2], rooted_hostname)) &&
					!(ns.isRunning(scripts[3], rooted_hostname))
				) {
					if ((ns.isRunning(scripts[0], rooted_hostname)) &&
						((threads_hack * ns.getScriptRam(scripts[3])) < ram_free)
						) {
						await ns.exec(scripts[3], rooted_hostname, threads_hack, target);
					}
					else if (!(ns.isRunning(scripts[0], rooted_hostname)) &&
						((threads_hack * ns.getScriptRam(scripts[3])) < ram_free)
						) {
						await ns.exec(scripts[2], rooted_hostname, threads_hack, target);
					}
				}
				if (
					(ns.getServerMoneyAvailable(target) < ns.getServerMaxMoney(target)) &&
					(ns.getServerSecurityLevel(target) <= ns.getServerMinSecurityLevel(target)) &&
					(threads_grow > 0) &&
					!(ns.isRunning(scripts[1], rooted_hostname))
				) {
					await ns.exec(scripts[1], rooted_hostname, threads_grow, target);
				}
			}
		}
		await ns.sleep(1);
	}
}