export async function main(ns) {
	await ns.sleep(((ns.getWeakenTime(ns.args[0]) - ns.getHackTime(ns.args[0])) * 1000) + 1); // 1 is just to ensure hack.js is definitely not executed before weaken.js finishes
	await ns.hack(ns.args[0]);
}