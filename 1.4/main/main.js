function servers_owned_all_max(ns, server_ram_max, servers_owned) {
	var tripwire = true;
	var i;
	if (servers_owned.length > 0) {
		for (i = 0; i < servers_owned.length; i++) {
			if (ns.getServerRam(servers_owned[i])[0] < server_ram_max) {
				tripwire = false;
			}
		}
	}
	return tripwire;
}

const return_server_bought_smallest = function (ns, servers_bought, server_ram_max) {
	let server_smallest;
	if (servers_bought.length > 0) {
		let size_smallest = server_ram_max;
		for (var index_0 = 0; index_0 < servers_bought.length; index_0++) {
			let size_of_server = ns.getServerRam(servers_bought[index_0])[0];
			if (size_of_server < size_smallest) {
				server_smallest = servers_bought[index_0];
				size_smallest = size_of_server;
			}
		}
	}
	return server_smallest;
};

function server_buy_best_afforded(ns, cash_per_ram, server_ram_min, server_ram_max, cash) {
	var server_ram_afforded = Math.pow(2, Math.trunc(Math.log2(cash / cash_per_ram) / Math.log2(2)));
	if (server_ram_afforded > server_ram_max) {
		server_ram_afforded = server_ram_max;
	}
	if (server_ram_afforded >= server_ram_min) {
		ns.purchaseServer("0", server_ram_afforded);
	}
}

function server_buy_or_upgrade(ns, cash_per_ram, server_ram_min, server_ram_max, cash, servers_owned, servers_owned_amount) {
	// if servers_owned.length <= 2 and your cash is >= the cheapest server
	if (
		(servers_owned_amount <= 24) &&
		(cash >= server_ram_min * cash_per_ram)
	) {
		server_buy_best_afforded(ns, cash_per_ram, server_ram_min, server_ram_max, cash);
	}
	if (
		(servers_owned_amount == 25) &&
		!(servers_owned_all_max(ns, server_ram_max, servers_owned))
	) {
		let server_owned_smallest = return_server_bought_smallest(ns, servers_owned, server_ram_max);
		let price_server_owned_smallest = cash_per_ram * ns.getServerRam(server_owned_smallest);
		// if cash is >= price of the cheapest server owned + the next highest server after that, which is twice the price of the former, thus 3
		if (cash >= 3 * price_server_owned_smallest) {
			ns.deleteServer(server_owned_smallest);
			server_buy_best_afforded(ns, cash_per_ram, server_ram_min, server_ram_max, cash);
		}
	}
}

function return_array_of_hostnames(ns, host) {
	let array_of_hostnames = [host];
	var i;
	for (i = 0; i < array_of_hostnames.length; i++) {
		let array_of_scan_results = ns.scan(array_of_hostnames[i]);
		var j;
		for (j = 0; j < array_of_scan_results.length; j++) {
			if (array_of_hostnames.indexOf(array_of_scan_results[j]) === -1) {
				array_of_hostnames.push(array_of_scan_results[j]);
			}
		}
	}
	return array_of_hostnames;
}

function return_array_of_unrooted_hostnames(ns, host, array_of_hostnames) {
	let array_of_unrooted_hostnames = [];
	var i;
	for (i = 0; i < array_of_hostnames.length; i++) {
		if (!ns.hasRootAccess(array_of_hostnames[i])) {
			array_of_unrooted_hostnames.push(array_of_hostnames[i]);
		}
	}
	return array_of_unrooted_hostnames;
}

function return_number_of_exploits(ns, host) {
	let number_of_exploits = 0;
	let array_of_exploits = ['BruteSSH.exe', 'FTPCrack.exe', 'relaySMTP.exe', 'HTTPWorm.exe', 'SQLInject.exe'];
	var i;
	for (i = 0; i < array_of_exploits.length; i++) {
		if (ns.fileExists(array_of_exploits[i], host)) {
			++number_of_exploits;
		}
	}
	return number_of_exploits;
}

function open_ports_nuke(ns, host, array_of_hostnames, array_of_unrooted_hostnames) {
	let number_of_exploits = return_number_of_exploits(ns, host);
	var i;
	for (i = 0; i < array_of_unrooted_hostnames.length; i++) {
		let unrooted_hostname = array_of_unrooted_hostnames[i];
		let number_of_ports_required = ns.getServerNumPortsRequired(unrooted_hostname);

		if (
			(number_of_ports_required <= number_of_exploits) &&
			(ns.getServerRequiredHackingLevel(unrooted_hostname) <= ns.getHackingLevel())
		) {
			if (number_of_ports_required > 4) {
				ns.sqlinject(unrooted_hostname);
				ns.httpworm(unrooted_hostname);
				ns.relaysmtp(unrooted_hostname);
				ns.ftpcrack(unrooted_hostname);
				ns.brutessh(unrooted_hostname);
			} else if (number_of_ports_required > 3) {
				ns.httpworm(unrooted_hostname);
				ns.relaysmtp(unrooted_hostname);
				ns.ftpcrack(unrooted_hostname);
				ns.brutessh(unrooted_hostname);
			} else if (number_of_ports_required > 2) {
				ns.relaysmtp(unrooted_hostname);
				ns.ftpcrack(unrooted_hostname);
				ns.brutessh(unrooted_hostname);
			} else if (number_of_ports_required > 1) {
				ns.ftpcrack(unrooted_hostname);
				ns.brutessh(unrooted_hostname);
			} else if (number_of_ports_required > 0) {
				ns.brutessh(unrooted_hostname);
			}
			ns.nuke(unrooted_hostname);
		}
	}
}

function return_array_of_rooted_hostnames(ns, host, array_of_hostnames) {
	let array_of_rooted_hostnames = [];
	var i;
	for (i = 0; i < array_of_hostnames.length; i++) {
		if (ns.hasRootAccess(array_of_hostnames[i])) {
			array_of_rooted_hostnames.push(array_of_hostnames[i]);
		}
	}
	return array_of_rooted_hostnames;
}

function return_target_best(ns, host, array_of_hostnames, array_of_rooted_hostnames) {
	var i;
	var target_best = array_of_rooted_hostnames[0];
	for (i = 0; i < array_of_rooted_hostnames.length; i++) {
		if (((ns.getServerMaxMoney(array_of_rooted_hostnames[i]) * ns.getServerGrowth(array_of_rooted_hostnames[i])) / ns.getServerBaseSecurityLevel(array_of_rooted_hostnames[i])) > ((ns.getServerMaxMoney(target_best) * ns.getServerGrowth(target_best)) / ns.getServerBaseSecurityLevel(target_best))) {
			target_best = array_of_rooted_hostnames[i];
		}
	}
	return target_best;
}

function return_total_free_ram_from_all_rooted_hostnames(ns, host, array_of_hostnames, array_of_rooted_hostnames) {
	var free_ram_from_all_rooted_hostnames = 0.0;
	var i;
	for (i = 0; i < array_of_rooted_hostnames.length; i++) {
		let res = ns.getServerRam(array_of_rooted_hostnames[i]);
		let ram_free = res[0] - res[1];
		free_ram_from_all_rooted_hostnames += ram_free;
	}
	return free_ram_from_all_rooted_hostnames;
}

function return_percentage_of_cash_from_available_per_hack(ns, server) { // adapted from scriptCalculatePercentMoneyHacked() in https://github.com/danielyxie/bitburner/blob/master/src/NetscriptEvaluator.js
	var difficultyMult = (100 - ns.getServerSecurityLevel(server)) / 100;
	var skillMult = (ns.getHackingLevel() - (ns.getServerRequiredHackingLevel(server) - 1)) / ns.getHackingLevel();
	var percentMoneyHacked = difficultyMult * skillMult * ns.getHackingMultipliers().money / 240;
	if (percentMoneyHacked < 0) {
		return 0;
	}
	if (percentMoneyHacked > 1) {
		return 1;
	}
	return percentMoneyHacked;
}

async function weaken_grow_hack(ns, host, array_of_hostnames, array_of_rooted_hostnames, target, percentage_of_cash_to_get) {
	let scripts = ["weaken.js", "grow.js", "hack.js", "hack_delayed.js"];
	var i;
	for (i = 0; i < array_of_rooted_hostnames.length; i++) {
		let rooted_hostname = array_of_rooted_hostnames[i];
		let res = ns.getServerRam(rooted_hostname);
		if (res[0] > 1) {
			let ram_free = res[0] - res[1];
			let total_ram_free = return_total_free_ram_from_all_rooted_hostnames(ns, host, array_of_hostnames, array_of_rooted_hostnames);
			let threads_weaken = Math.trunc(ram_free / ns.getScriptRam(scripts[0]));
			let threads_grow = Math.trunc(ram_free / ns.getScriptRam(scripts[1]));
			let threads_hack_max = Math.trunc(ram_free / ns.getScriptRam(scripts[2])); // max hack
			var threads_hack_percentage = Math.trunc((Math.pow(threads_hack_max, 2) * percentage_of_cash_to_get) / (return_percentage_of_cash_from_available_per_hack(ns, target) * Math.pow(Math.trunc(total_ram_free / ns.getScriptRam(scripts[2])), 2))); // percentage hack, see notes.txt for how formula was derived
			if (threads_hack_percentage < 1) {
				threads_hack_percentage = 1;
			}
			var threads_hack = 0;
			if ((threads_hack_percentage) > (threads_hack_max)) {
				threads_hack = threads_hack_max;
			} else {
				threads_hack = threads_hack_percentage;
			}

			let threads_hack_delayed_max = Math.trunc(ram_free / ns.getScriptRam(scripts[3])); // max hack_delayed
			var threads_hack_delayed_percentage = Math.trunc((Math.pow(threads_hack_delayed_max, 2) * percentage_of_cash_to_get) / (return_percentage_of_cash_from_available_per_hack(ns, target) * Math.pow(Math.trunc(total_ram_free / ns.getScriptRam(scripts[3])), 2))); // percentage hack_delayed, see notes.txt for how formula was derived
			if (threads_hack_delayed_percentage < 1) {
				threads_hack_delayed_percentage = 1;
			}
			var threads_hack_delayed = 0;
			if ((threads_hack_delayed_percentage) > (threads_hack_delayed_max)) {
				threads_hack_delayed = threads_hack_delayed_max;
			} else {
				threads_hack_delayed = threads_hack_delayed_percentage;
			}

			ns.scp(scripts, host, rooted_hostname);
			if (
				(ns.getServerSecurityLevel(target) > ns.getServerMinSecurityLevel(target)) &&
				(threads_weaken > 0) &&
				!(ns.isRunning(scripts[0], rooted_hostname))
			) {
				await ns.exec(scripts[0], rooted_hostname, threads_weaken, target);
			}
			if (
				(ns.getServerMoneyAvailable(target) >= ns.getServerMaxMoney(target)) &&
				(ns.getServerSecurityLevel(target) <= ns.getServerMinSecurityLevel(target) + (0.05 * Math.trunc(total_ram_free / ns.getScriptRam(scripts[0])))) && // 0.05 is the amount weaken() decreases security by
				(threads_hack > 0) &&
				!(ns.isRunning(scripts[1], rooted_hostname)) &&
				!(ns.isRunning(scripts[2], rooted_hostname)) &&
				!(ns.isRunning(scripts[3], rooted_hostname))
			) {
				if ((ns.isRunning(scripts[0], rooted_hostname)) &&
					((threads_hack_delayed * ns.getScriptRam(scripts[3])) < ram_free)
				) {
					await ns.exec(scripts[3], rooted_hostname, threads_hack_delayed, target);
				}
				if ((!ns.isRunning(scripts[0], rooted_hostname)) &&
					((threads_hack * ns.getScriptRam(scripts[2])) < ram_free)
				) {
					await ns.exec(scripts[2], rooted_hostname, threads_hack, target);
				}
			}
			if (
				(ns.getServerMoneyAvailable(target) < ns.getServerMaxMoney(target)) &&
				(ns.getServerSecurityLevel(target) <= ns.getServerMinSecurityLevel(target)) &&
				(threads_grow > 0) &&
				!(ns.isRunning(scripts[1], rooted_hostname))
			) {
				await ns.exec(scripts[1], rooted_hostname, threads_grow, target);
			}
		}
	}
}

export async function main(ns) {
	const cash_per_ram = 55000;
	const server_ram_min_log2 = 1;
	const server_ram_max_log2 = 20;
	const server_ram_min = Math.pow(2, server_ram_min_log2);
	const server_ram_max = Math.pow(2, server_ram_max_log2);
	const host = "home" // or ns.getHostname();
	const conf_file = "conf.txt"
	var target;
	var percentage_of_cash_to_get;
	while (true) {
		// can't be constant as new servers are added after buying
		let array_of_hostnames = return_array_of_hostnames(ns, host);
		let array_of_unrooted_hostnames = return_array_of_unrooted_hostnames(ns, host, array_of_hostnames);
		let array_of_rooted_hostnames = return_array_of_rooted_hostnames(ns, host, array_of_hostnames);
		
		open_ports_nuke(ns, host, array_of_hostnames, array_of_unrooted_hostnames);
		
		let cash = ns.getServerMoneyAvailable(host);
		let servers_owned = ns.getPurchasedServers();
		let servers_owned_amount = servers_owned.length;
		
 		// if no arguments given, use conf_file
		if (ns.args[0] === undefined) {
			var conf;
			conf = ns.read(conf_file).split(" ");
			// if conf_file empty, throw error
			if (conf[0] === undefined) {
				throw "No arguments given.";
			// if conf_file not empty
			} else {
				// if only one argument in file, use that as percentage_of_cash_to_get, do not buy/replace servers, automatically target best servers
				if (conf[1] === undefined) {
					percentage_of_cash_to_get = conf[0];
					target = return_target_best(ns, host, array_of_hostnames, array_of_rooted_hostnames);
					await weaken_grow_hack(ns, host, array_of_hostnames, array_of_rooted_hostnames, target, percentage_of_cash_to_get);
				} else {
					// if only two arguments are in the file, use first as percentage_of_cash_to_get, use second to determine if servers are bought or replaced, auto-target is activate
					if (conf[2] === undefined) {
						percentage_of_cash_to_get = conf[0];
						if (conf[1] != "true") {
							percentage_of_cash_to_get = conf[0];
							target = return_target_best(ns, host, array_of_hostnames, array_of_rooted_hostnames);
							await weaken_grow_hack(ns, host, array_of_hostnames, array_of_rooted_hostnames, target, percentage_of_cash_to_get);
						} else {
							percentage_of_cash_to_get = conf[0];
							server_buy_or_upgrade(ns, cash_per_ram, server_ram_min, server_ram_max, cash, servers_owned, servers_owned_amount);
							target = return_target_best(ns, host, array_of_hostnames, array_of_rooted_hostnames);
							await weaken_grow_hack(ns, host, array_of_hostnames, array_of_rooted_hostnames, target, percentage_of_cash_to_get);
						}
					// otherwise, there must be three or more arguments in the file
					} else {
						if (conf[1] != "true") {
							percentage_of_cash_to_get = conf[0];
							target = conf[2];
							await weaken_grow_hack(ns, host, array_of_hostnames, array_of_rooted_hostnames, target, percentage_of_cash_to_get);
						} else {
							percentage_of_cash_to_get = conf[0];
							server_buy_or_upgrade(ns, cash_per_ram, server_ram_min, server_ram_max, cash, servers_owned, servers_owned_amount);
							target = conf[2];
							await weaken_grow_hack(ns, host, array_of_hostnames, array_of_rooted_hostnames, target, percentage_of_cash_to_get);
						}
					}
				}
			}
		// arguments were given, don't use conf_file
		} else {
			// if only one argument given, use that as percentage_of_cash_to_get, do not buy/replace servers, automatically target best servers
			if (ns.args[1] === undefined) {
				percentage_of_cash_to_get = ns.args[0];
				target = return_target_best(ns, host, array_of_hostnames, array_of_rooted_hostnames);
				await weaken_grow_hack(ns, host, array_of_hostnames, array_of_rooted_hostnames, target, percentage_of_cash_to_get);
			} else {
				// if only two arguments given, use first as percentage_of_cash_to_get, use second to determine if servers are bought or replaced, auto-target is activate
				if (ns.args[2] === undefined) {
					percentage_of_cash_to_get = ns.args[0];
					if (ns.args[1] != "true") {
						percentage_of_cash_to_get = ns.args[0];
						target = return_target_best(ns, host, array_of_hostnames, array_of_rooted_hostnames);
						await weaken_grow_hack(ns, host, array_of_hostnames, array_of_rooted_hostnames, target, percentage_of_cash_to_get);
					} else {
						percentage_of_cash_to_get = ns.args[0];
						server_buy_or_upgrade(ns, cash_per_ram, server_ram_min, server_ram_max, cash, servers_owned, servers_owned_amount);
						target = return_target_best(ns, host, array_of_hostnames, array_of_rooted_hostnames);
						await weaken_grow_hack(ns, host, array_of_hostnames, array_of_rooted_hostnames, target, percentage_of_cash_to_get);
					}
				// otherwise, there must be three or more arguments in the file
				} else {
					if (ns.args[1] != "true") {
						percentage_of_cash_to_get = ns.args[0];
						target = ns.args[2];
						await weaken_grow_hack(ns, host, array_of_hostnames, array_of_rooted_hostnames, target, percentage_of_cash_to_get);
					} else {
						percentage_of_cash_to_get = ns.args[0];
						server_buy_or_upgrade(ns, cash_per_ram, server_ram_min, server_ram_max, cash, servers_owned, servers_owned_amount);
						target = ns.args[2];
						await weaken_grow_hack(ns, host, array_of_hostnames, array_of_rooted_hostnames, target, percentage_of_cash_to_get);
					}
				}
			}
		}
		await ns.sleep(1);
	} 
}