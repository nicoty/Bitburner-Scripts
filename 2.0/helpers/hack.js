export const main = async function (ns) {
	await ns.sleep(ns.args[3]);
	const start = Date.now();
	const s = new Date(start);
	await ns.hack(ns.args[0]);
	const finish = Date.now();
	const f = new Date(finish);
	ns.tprint("\t#" + ns.args[1] + "\t" + ns.args[2] + " hacking " + ns.args[0] + "\nHack started:\t\t" + s.toISOString() + "\nHack finished:\t\t" + f.toISOString());
};