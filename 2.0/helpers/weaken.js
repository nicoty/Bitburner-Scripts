export const main = async function (ns) {
	const start = Date.now();
	const s = new Date(start);
	await ns.weaken(ns.args[0]);
	const finish = Date.now();
	const f = new Date(finish);
	ns.tprint("\t#" + ns.args[1] + "\t" + ns.args[2] + " weakening " + ns.args[0] + "\nWeaken started:\t\t" + s.toISOString() + "\nWeaken finished:\t" + f.toISOString());
};