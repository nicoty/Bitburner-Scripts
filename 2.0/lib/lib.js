//	returns an array of all servers in the game
export const return_array_of_servers = function (ns, host) {
	const array_of_servers = [host];
	for (let indices_0 = 0; indices_0 < array_of_servers.length; ++indices_0) {
		const array_of_scan_results = ns.scan(array_of_servers[indices_0]);
		for (let indices_1 = 0; indices_1 < array_of_scan_results.length; ++indices_1) {
			if (array_of_servers.indexOf(array_of_scan_results[indices_1]) === -1) {
				array_of_servers.push(array_of_scan_results[indices_1]);
			}
		}
	}
	return array_of_servers;
};

//	returns the total ram of a server
const return_server_ram_total = function (ns, server) {
	return (ns.getServerRam(server))[0];
};

//	returns the used ram of a server
const return_server_ram_used = function (ns, server) {
	return (ns.getServerRam(server))[1];
};

//	returns the amount of free ram of a server
const return_server_ram_free = function (ns, server) {
	return return_server_ram_total(ns, server) - return_server_ram_used(ns, server);
};

//	buy/upgrade servers

//	true if all bought servers have ram == server_ram_max
const servers_bought_all_max = function (ns, server_ram_max) {
	const servers_bought = ns.getPurchasedServers();
	if (servers_bought.length > 0) {
		let tripwire = true;
		for (let indices_0 = 0; indices_0 < servers_bought.length; ++indices_0) {
			if (return_server_ram_total(ns, servers_bought[indices_0]) < server_ram_max) {
				tripwire = false;
			}
		}
		return tripwire;
	}
};

//	returns bought server with smallest RAM
const return_server_bought_smallest = function (ns, server_ram_max) {
	const servers_bought = ns.getPurchasedServers();
	if (servers_bought.length > 0) {
		let server_smallest;
		let size_smallest = server_ram_max;
		for (let indices_0 = 0; indices_0 < servers_bought.length; ++indices_0) {
			let size_of_server = return_server_ram_total(ns, servers_bought[indices_0]);
			if (size_of_server < size_smallest) {
				server_smallest = servers_bought[indices_0];
				size_smallest = size_of_server;
			}
		}
		return server_smallest;
	}
};

//	returns bought server with biggest RAM
const return_server_bought_biggest = function (ns, server_ram_min) {
	const servers_bought = ns.getPurchasedServers();
	if (servers_bought.length > 0) {
		let server_biggest;
		let size_biggest = server_ram_min;
		for (let indices_0 = 0; indices_0 < servers_bought.length; ++indices_0) {
			let size_of_server = return_server_ram_total(ns, servers_bought[indices_0]);
			if (size_of_server > size_biggest) {
				server_biggest = servers_bought[indices_0];
				size_biggest = size_of_server;
			}
		}
	return server_biggest;
	}
};

//	return the RAM of the server with the biggest RAM that you can afford
const return_server_ram_biggest_afforded = function (ns, cash_per_ram, server_ram_max, host) {
	let server_ram_biggest_afforded = Math.pow(2, Math.trunc(Math.log2(ns.getServerMoneyAvailable(host) / cash_per_ram) / Math.log2(2)));
	if (server_ram_biggest_afforded > server_ram_max) {
		server_ram_biggest_afforded = server_ram_max;
	}
	return server_ram_biggest_afforded;
};

//	buys the best server with cash available, unless there are 25 servers already, in which case deletes the worst server, unless all 25 have ram == server_ram_max
export const server_buy_or_upgrade = function (ns, cash_per_ram, server_amount_max, server_ram_min, server_ram_max, host, name) {
	const servers_bought = ns.getPurchasedServers();
//	if servers_bought.length <= 2 and your cash is >= the cheapest server
	const server_ram_biggest_afforded = return_server_ram_biggest_afforded(ns, cash_per_ram, server_ram_max, host);
//	you currently have no bought servers yet, get a server with ram at least equal to "home"
	if (
//		ram is at least equal to the minimum ram possible for purchased servers
		(server_ram_biggest_afforded >= server_ram_min) &&
//		ram is at least equal to the ram of "home", probably a bad idea to hardcode this since the name of "home" might change in the future. tags: warning, potential, problem
		(server_ram_biggest_afforded >= return_server_ram_total(ns, host)) &&
//		you have no bought servers yet
		(servers_bought.length === 0)
	) {
		ns.purchaseServer(name, server_ram_biggest_afforded);
	}
	
//	you have one or more servers already
	if ((servers_bought.length > 0)) {
//		get smallest bought server
		const server_bought_smallest = return_server_bought_smallest(ns, server_ram_max);
//		get biggest bought server
		const server_bought_biggest = return_server_bought_biggest(ns, server_ram_min);
//		get the ram of the smallest bought server
		const ram_server_bought_biggest = return_server_ram_total(ns, server_bought_biggest);
		
//		you currently own less than the maximum amount of purchased servers allowed and you dont own a server with max ram yet, buy a server with ram greater than the ram of your biggest bought server
		if (
//			you currently own less than the maximum amount of purchased servers allowed
			(servers_bought.length < server_amount_max) &&
//			your biggest bought server doesn't have the max ram possible
			(ram_server_bought_biggest != server_amount_max) &&
//			ram of server to be bought is greater the ram of your biggest bought server
			(server_ram_biggest_afforded > ram_server_bought_biggest)
		) {
			ns.purchaseServer(name, server_ram_biggest_afforded);
		}
		
//		you currently own less than the maximum amount of purchased servers allowed and you already bought a server with max ram, buy another server the max possible ram
		if (
//			you currently own less than the maximum amount of purchased servers allowed
			(servers_bought.length < server_amount_max) &&
//			your biggest bought server has the max ram possible
			(ram_server_bought_biggest == server_ram_max) &&
//			ram of server to be bought is greater the ram of your biggest bought server
			(server_ram_biggest_afforded == server_ram_max)
		) {
			ns.purchaseServer(name, server_ram_biggest_afforded);
		}
		
		if (
//			you currently own the maximum amount of purchased servers allowed
			(servers_bought.length == server_amount_max) &&
//			your servers do not all have the maximum ram possible for purchased servers
			!(servers_bought_all_max(ns, server_ram_max))
		) {
			const price_server_bought_biggest = cash_per_ram * ram_server_bought_biggest;
//			is cash at least equal to the price of the cheapest server bought + the next highest server after that, which is twice the price of the former, thus 3. this check is so that a server with the same ram as before isn't bought.
			if (ns.getServerMoneyAvailable(host) >= 3 * price_server_bought_biggest) {
				ns.deleteServer(server_bought_smallest);
				ns.purchaseServer(name, server_ram_biggest_afforded);
			}
		}
	}
};


//	root servers

//	returns an array of all servers that are yet to be rooted
export const return_array_of_unrooted_servers = function (ns, array_of_servers) {
	const array_of_unrooted_servers = [];
	for (let indices_0 = 0; indices_0 < array_of_servers.length; ++indices_0) {
		if (!ns.hasRootAccess(array_of_servers[indices_0])) {
			array_of_unrooted_servers.push(array_of_servers[indices_0]);
		}
	}
	return array_of_unrooted_servers;
};

//	returns the number of exploits present in the host
const return_number_of_exploits = function (ns, array_of_exploits, host) {
	let number_of_exploits = 0;
	for (let indices_0 = 0; indices_0 < array_of_exploits.length; ++indices_0) {
		if (ns.fileExists(array_of_exploits[indices_0], host)) {
			++number_of_exploits;
		}
	}
	return number_of_exploits;
};

//	opens ports of servers in an array using available exploits
const open_ports = function (ns, array_of_exploits, host) {
	for (let indices_0 = 0; indices_0 < programs.length; ++indices_0) {
		if (fileExists(array_of_exploits[indices_0], host)) {
			if (indices_0 === 0) {brutessh(target);}
			if (indices_0 === 1) {ftpcrack(target);}
			if (indices_0 === 2) {relaysmtp(target);}
			if (indices_0 === 3) {httpworm(target);}
			if (indices_0 === 4) {sqlinject(target);}
		}
	}
};

//	opens ports and nukes any unrooted servers if the player's hacking level is high enough to do so and the appropriate number of exploits are present
export const open_ports_nuke = function (ns, array_of_exploits, host, array_of_unrooted_servers) {
	for (let indices_0 = 0; indices_0 < array_of_unrooted_servers.length; ++indices_0) {
		const unrooted_server = array_of_unrooted_servers[indices_0];
		if (
			(ns.getServerRequiredHackingLevel(unrooted_server) <= ns.getHackingLevel()) &&
			(ns.getServerNumPortsRequired(unrooted_server) <= return_number_of_exploits(ns, array_of_exploits, host))
		) {
			open_ports(ns, array_of_exploits, host);
			ns.nuke(unrooted_server);
		}
	}
};


//	targetting

//	sort an array of servers by their amounts of RAM, from lowest to highest
export const sort_by_server_ram = function (ns, array_of_servers) {
	return array_of_servers.sort((element_0, element_1) => (return_server_ram_total(ns, element_0) - return_server_ram_total(ns, element_1)));
};

//	returns the score of a server which is calculated by taking into account its max cash, growth, minimum security level and server difficulty
const server_score = function (ns, server_target) {
	return ns.getServerMaxMoney(server_target) * ns.getServerGrowth(server_target) / ns.getServerMinSecurityLevel(server_target) * ns.getServerRequiredHackingLevel(server_target);
};

//	sort an array of servers by their server_score, from lowest to highest
export const sort_by_server_scores = function (ns, array_of_servers) {
	return array_of_servers.sort((element_0, element_1) => server_score(ns, element_0) - server_score(ns, element_1));
};


//	weaken, grow, hack

//	returns an array of all rooted servers
export const return_array_of_rooted_servers = function (ns, array_of_servers) {
	const array_of_rooted_servers = [];
	for (let indices_0 = 0; indices_0 < array_of_servers.length; ++indices_0) {
		if (ns.hasRootAccess(array_of_servers[indices_0])) {
			array_of_rooted_servers.push(array_of_servers[indices_0]);
		}
	}
	return array_of_rooted_servers;
};

//	returns the sum of all free ram from all the servers you have root access to
const return_ram_free_total = function (ns, array_of_rooted_servers) {
	let free_ram_from_all_rooted_servers = 0.0;
	for (let indices_0 = 0; indices_0 < array_of_rooted_servers.length; ++indices_0) {
		free_ram_from_all_rooted_servers += return_server_ram_free(ns, array_of_rooted_servers[indices_0]);
	}
	return free_ram_from_all_rooted_servers;
};

//	returns what percentage of ram_free_total the ram of a server is
const return_percentage_of_server_ram_from_total = function (ns, server, ram_free_total) {
	return return_server_ram_free(ns, server) / ram_free_total;
};

//	returns threads_required if its less than or equal to threads_available, otherwise returns threads_available
const return_corrected_threads = function (ns, threads_required, threads_available) {
	if (threads_required > threads_available) {
		return threads_available;
	} else {
		return threads_required;
	}
};

//	weaken stuff

//	 returns the threads required for weaken to cause a server_target's security to reach minimum
const return_threads_required_for_min_security_weaken = function (ns, security_decrease_weaken, server_target, server_target_current_security) {
	return Math.ceil((server_target_current_security - ns.getServerMinSecurityLevel(server_target)) / security_decrease_weaken);
};

//	 returns the threads required for weaken to cause a server_target's security to reach minimum if possible, otherwise, return max threads that server_used can provide
const return_threads_weaken = function (ns, security_decrease_weaken, ram_script_weaken, server_used, server_target, server_target_current_security) {
	const threads_available = Math.trunc(return_server_ram_free(ns, server_used) / ram_script_weaken);
	const threads_required = return_threads_required_for_min_security_weaken(ns, security_decrease_weaken, server_target, server_target_current_security);
	return return_corrected_threads(ns, threads_required, threads_available);
};

//	grow stuff

//	returns the threads required for grow to cause a server_target's cash to grow by the variable "growth" which is a percentage, assuming minimum security. Adapted from numCycleForGrowth in https://github.com/danielyxie/bitburner/blob/master/src/Server.js
const return_threads_required_for_growth_at_min_security = function (ns, cash_percentage_increase_base_grow, cash_percentage_increase_max_grow, multipliers, server_target, growth) {
	let ajdGrowthRate = 1 + (cash_percentage_increase_base_grow - 1) / ns.getServerMinSecurityLevel(server_target);
	if(ajdGrowthRate > cash_percentage_increase_max_grow) {
		ajdGrowthRate = cash_percentage_increase_max_grow;
	}

	const serverGrowthPercentage = ns.getServerGrowth(server_target) / 100;

	const cycles = Math.log(growth) / (Math.log(ajdGrowthRate) * multipliers.growth * serverGrowthPercentage);
	return cycles;
};

//	returns the threads required by grow to grow a server_target's cash to its maximum when at minimum security
const return_threads_required_for_max_cash_grow = function (ns, cash_percentage_increase_base_grow, cash_percentage_increase_max_grow, multipliers, server_target) {
	return return_threads_required_for_growth_at_min_security(ns, cash_percentage_increase_base_grow, cash_percentage_increase_max_grow, multipliers, server_target, ns.getServerMaxMoney(server_target) / ns.getServerMoneyAvailable(server_target));
};

//	 returns the threads required by grow to grow a server_target's cash to its maximum if possible, otherwise, return max threads that server_used can provide. assumes minimum security
const return_threads_grow = function (ns, cash_percentage_increase_base_grow, cash_percentage_increase_max_grow, multipliers, ram_script_grow, server_used, server_target) {
	const threads_available = Math.trunc(return_server_ram_free(ns, server_used) / ram_script_grow);
	const threads_required = return_threads_required_for_max_cash_grow(ns, cash_percentage_increase_base_grow, cash_percentage_increase_max_grow, multipliers, server_target);
	return return_corrected_threads(ns, threads_required, threads_available);
};

//	hack stuff

//	returns the percentage of the available cash in a server_target that is stolen when it is hacked. assumes minimum security
const return_percentage_of_cash_from_available_per_hack = function (ns, multipliers, server_target) { //	adapted from scriptCalculatePercentMoneyHacked() in https://github.com/danielyxie/bitburner/blob/master/src/NetscriptEvaluator.js . See also `hackDifficulty` in https://github.com/danielyxie/bitburner/blob/master/src/Server.js
	const difficultyMult = (100 - ns.getServerMinSecurityLevel(server_target)) / 100;
	const skillMult = (ns.getHackingLevel() - (ns.getServerRequiredHackingLevel(server_target) - 1)) / ns.getHackingLevel();
	let percentMoneyHacked = difficultyMult * skillMult * multipliers.money / 240;
	if (percentMoneyHacked < 0) {
		return 0;
	}
	if (percentMoneyHacked > 1) {
		return 1;
	}
	return percentMoneyHacked; //	* BitNodeMultipliers.ScriptHackMoney;
};

//	returns the threads required to steal "percentage_to_steal" of available money in a server_target, assuming minimum security
const return_threads_required_to_hack_percentage = function (ns, multipliers, percentage_to_steal, server_target) {
	return percentage_to_steal / return_percentage_of_cash_from_available_per_hack(ns, multipliers, server_target);
};

//	returns the threads required to steal "percentage_to_steal" of available money in a server_target if possible, otherwise, return max threads that server_used can provide. assumes minimum security
const return_threads_hack = function (ns, multipliers, ram_script_hack, server_used, percentage_to_steal, server_target) {
	const threads_available = Math.trunc(return_server_ram_free(ns, server_used) / ram_script_hack);
	const threads_required = return_threads_required_to_hack_percentage(ns, multipliers, percentage_to_steal, server_target);
	return return_corrected_threads(ns, threads_required, threads_available);
};

//	percentage to steal stuff

//	returns the threads required by grow to grow a server_target's money by percentage_to_steal, assuming minimum security
const return_threads_required_for_max_cash_grow_at_min_security = function (ns, cash_percentage_increase_base_grow, cash_percentage_increase_max_grow, multipliers, server_target, percentage_to_steal) {
	return return_threads_required_for_growth_at_min_security(ns, cash_percentage_increase_base_grow, cash_percentage_increase_max_grow, multipliers, server_target, 1 / (1 - percentage_to_steal));
};

//	returns true if there is enough ram to provide the threads required by grow to grow a server_target's cash by percentage_to_steal if possible, otherwise, returns false. assumes minimum security
const is_ram_enough_for_percentage = function (ns, cash_percentage_increase_base_grow, cash_percentage_increase_max_grow, multipliers, ram_script_grow, server_used, server_target, percentage_to_steal) {
	const threads_available = Math.trunc(return_server_ram_free(ns, server_used) / ram_script_grow);
	const threads_required = return_threads_required_for_max_cash_grow_at_min_security(ns, cash_percentage_increase_base_grow, cash_percentage_increase_max_grow, multipliers, server_target, percentage_to_steal);
	if (threads_required < threads_available) {
		return false;
	} else {
		return true;
	}
};

//	returns the number of cycles of bisection to be done to reach a certain precision, to the rounded up to the nearest integer
const return_cycles_for_bisection_precision = function (ns, precision) {
	return Math.ceil(Math.log(1 / precision) / Math.log(2));
};

//	returns optimum percentage to steal such that cash stolen is as high as it can be and target's cash still able to be grown to 100% after one grow with the ram it has available
const return_percentage_to_steal = function (ns, cash_percentage_increase_base_grow, cash_percentage_increase_max_grow, multipliers, ram_script_grow, server_used, server_target, precision) {
	let ceiling = 1;
	let floor = 0;
	let percentage_to_steal = (ceiling + floor) / 2;
	for (let indices_0 = 0; indices_0 < return_cycles_for_bisection_precision(ns, precision); ++indices_0) {
		if (is_ram_enough_for_percentage(ns, cash_percentage_increase_base_grow, cash_percentage_increase_max_grow, multipliers, ram_script_grow, server_used, server_target, percentage_to_steal) === true) {
			ceiling = percentage_to_steal;
		} else {
			floor = percentage_to_steal;
		}
		percentage_to_steal = (ceiling + floor) / 2;
	}
	// cap to 0.99 so not all money is stolen, which is bad because it's harder to grow from 0 in most cases
	if (percentage_to_steal < 0.99) {
		return percentage_to_steal;
	} else {
		return 0.99;
	}
};

//	copy stuff

//	copies files to all rooted servers
export const copy_files_to_rooted_servers = function (ns, files, source, array_of_rooted_servers) {
	for (let indices_0 = 0; indices_0 < array_of_rooted_servers.length; ++indices_0) {
		ns.scp(files, source, array_of_rooted_servers[indices_0]);
	}
};

//	actual scheduler stuff

//	execute the "weaken.js" script
const exec_script_weaken = async function (ns, script, server_used, threads, server_target, identifier) {
//	parameters = weaken script name, host being used, threads to use, server_target, identifier to display instance when running, host being used to be displayed when running
	await ns.exec(script, server_used, threads, server_target, identifier, server_used);
};

//	execute the "grow.js" script
const exec_script_grow = async function (ns, script, server_used, threads, server_target, identifier) {
//	parameters = grow script name, host being used, threads to use, server_target, identifier to display instance when running, host being used to be displayed when running, sleep duration before grow
	await ns.exec(script, server_used, threads, server_target, identifier, server_used, Math.round(1000 * (ns.getWeakenTime(server_target) - ns.getGrowTime(server_target))));
};

//	execute the "hack.js" script
const exec_script_hack = async function (ns, script, server_used, threads, server_target, identifier) {
//	parameters = hack script name, host being used, threads to use, server_target, identifier to display instance when running, host being used to be displayed when running, sleep duration before hack
	await ns.exec(script, server_used, threads, server_target, identifier, server_used, Math.round(1000 * (ns.getWeakenTime(server_target) - ns.getHackTime(server_target))));
};

const run_weaken = async function (ns, script, security_decrease_weaken, ram_script_weaken, server_used, server_target, server_target_current_security, padding, identifier) {
	let threads = return_threads_weaken(ns, security_decrease_weaken, ram_script_weaken, server_used, server_target, server_target_current_security);
	if (
		(threads < 1)
	) {
		threads = 1;
	}
	await exec_script_weaken(ns, script, server_used, threads, server_target, identifier);
	await ns.sleep(padding);
}

//	scheduler. assumes `weaken` takes the longest to resolve
export const weaken_grow_hack = async function (ns, security_decrease_weaken, security_increase_grow, security_increase_hack, cash_percentage_increase_base_grow, cash_percentage_increase_max_grow, multipliers, array_of_scripts, ram_script_weaken, ram_script_grow, ram_script_hack, server_used, server_target, server_target_security, padding, precision) {
//	weaken #1
	let server_target_current_security = server_target_security;
	if (server_target_security < 1) {
		server_target_current_security = 1;
	} else {
		server_target_current_security = server_target_security;
	}
	const threads_weaken_0 = return_threads_weaken(ns, security_decrease_weaken, ram_script_weaken, server_used, server_target, server_target_security);
	await run_weaken(ns, array_of_scripts[0], security_decrease_weaken, ram_script_weaken, server_used, server_target, server_target_security, padding, "a");
	server_target_current_security -= security_decrease_weaken * threads_weaken_0;


//	grow
	let threads_grow = return_threads_grow(ns, cash_percentage_increase_base_grow, cash_percentage_increase_max_grow, multipliers, ram_script_grow, server_used, server_target);
	if (
		(threads_grow < 1)
	) {
		threads_grow = 1;
	}
	if (
//		only run if there's any free ram
		(threads_grow > 0)
	) {
		await exec_script_grow(ns, array_of_scripts[1], server_used, 1000, server_target, "b");
		await ns.sleep(padding);
		server_target_current_security += security_increase_grow * threads_grow;
	}

//	weaken #2
	const threads_weaken_1 = return_threads_weaken(ns, security_decrease_weaken, ram_script_weaken, server_used, server_target, server_target_current_security);
	await run_weaken(ns, array_of_scripts[0], security_decrease_weaken, ram_script_weaken, server_used, server_target, server_target_current_security, padding, "c");
	server_target_current_security -= security_decrease_weaken * threads_weaken_1;
	
//	hack
	const server_used_total_ram = return_server_ram_total(ns, server_used);
//	fuck yeah bitches. managed to make the percentage to steal calculator work. and w0t m8
	const percentage_to_steal = return_percentage_to_steal(ns, cash_percentage_increase_base_grow, cash_percentage_increase_max_grow, multipliers, ram_script_grow, server_used, server_target, precision);
	let threads_hack = return_threads_hack(ns, multipliers, ram_script_hack, server_used, percentage_to_steal, server_target);
//	only run if server_target's money is maximum and if there's any free ram and if the hack script isn't currently running (to prevent running twice. this is a naive check though because its possible for to valid instances to run if weaken and grow resolve quick enough)
	if (
		(threads_hack < 1)
	) {
		threads_hack = 1;
	}
	if (
		(threads_hack > 0)
	) {
		await exec_script_hack(ns, array_of_scripts[2], server_used, threads_hack, server_target, "d");
		await ns.sleep(padding);
		server_target_current_security += security_increase_hack * threads_hack;
	}
	await ns.sleep(1);
	return server_target_current_security;
};