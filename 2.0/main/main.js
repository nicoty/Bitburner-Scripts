import {
	return_array_of_servers,
	server_buy_or_upgrade,
	return_array_of_unrooted_servers,
	open_ports_nuke,
	return_array_of_rooted_servers,
	sort_by_server_ram,
	sort_by_server_scores,
	copy_files_to_rooted_servers,
	weaken_grow_hack
} from "lib.js";

// constants are from https://github.com/danielyxie/bitburner/blob/master/src/Constants.js
export const main = async function (ns) {
//	cost of server per 1 GB of RAM
	const cash_per_ram = 55000;
//	maximum amount of purchased servers allowed
	const server_amount_max = 25;
//	log2 of minimum ram of purchased servers possible
	const server_ram_min_log2 = 1;
//	log2 of maximum ram of purchased servers possible
	const server_ram_max_log2 = 20;
//	minimum ram of purchased servers possible
	const server_ram_min = Math.pow(2, server_ram_min_log2);
//	maximum ram of purchased servers possible
	const server_ram_max = Math.pow(2, server_ram_max_log2);
//	amount security increases by from a successful hack
	const security_increase_hack = 0.002;
//	amount security increases by from a grow
	const security_increase_grow = 0.004;
//	amount security decreases by from a weaken
	const security_decrease_weaken = 0.05;
//	base percentage cash increases by from a grow
	const cash_percentage_increase_base_grow = 1.03;
//	max percentage cash increases by from a grow (accounts for server security)
	const cash_percentage_increase_max_grow = 1.0035;
/*	player multipliers. has the structure
	{
		chance: Player's hacking chance multiplier,
		speed: Player's hacking speed multiplier,
		money: Player's hacking money stolen multiplier,
		growth: Player's hacking growth multiplier
	}
	use with eg multipliers.chance
*/
	const multipliers = ns.getHackingMultipliers();
//	array of exploits used to open ports
	const array_of_exploits = ["BruteSSH.exe", "FTPCrack.exe", "relaySMTP.exe","HTTPWorm.exe", "SQLInject.exe"];
//	array of helper scripts
	const scripts = ["weaken.js", "grow.js", "hack.js"];
//	ram cost of "weaken.js"
	const ram_script_weaken = 1.75; // or ns.getScriptRam(scripts[0]);
//	ram cost of "grow.js"
	const ram_script_grow = 1.75; // or ns.getScriptRam(scripts[1]);
//	ram cost of "hack.js"
	const ram_script_hack = 1.7; // or ns.getScriptRam(scripts[2]);
//	name of current host (usually "home")
	const host = "home"; // or ns.getHostname()
//	name of purchased servers
	const name = "0";
	while (true) {
//		array of current servers in the game (including bought servers)
		let array_of_servers = return_array_of_servers(ns, host);
//		buys the best server with cash available, unless there are 25 servers already, in which case deletes the worst server, unless all 25 have ram == server_ram_max
		server_buy_or_upgrade(ns, cash_per_ram, server_amount_max, server_ram_min, server_ram_max, host, name);

//		array of current unrooted servers in the game
		const array_of_unrooted_servers = return_array_of_unrooted_servers(ns, array_of_servers);
//		opens ports and nukes any unrooted servers if the player's hacking level is high enough to do so and the appropriate number of exploits are present
		open_ports_nuke(ns, array_of_exploits, host, array_of_unrooted_servers);

//		array of current servers in the game (including bought servers) to be used for the loop
		let array_of_rooted_servers_used = return_array_of_rooted_servers(ns, array_of_servers);
//		array of current servers in the game (including bought servers) to be targetterd
		let array_of_rooted_servers_targetted = return_array_of_rooted_servers(ns, array_of_servers); 
//		sorts the array of rooted servers by their ram, from lowest to highest
		sort_by_server_ram(ns, array_of_rooted_servers_used);
//		sorts the array of rooted servers by their server_score, from lowest to highest
		sort_by_server_scores(ns, array_of_rooted_servers_targetted);
//		copy scripts to rooted servers
		copy_files_to_rooted_servers(ns, scripts, host, array_of_rooted_servers_used);
//		how long should sleep duration be
		const padding = 5000;
//		how precise the percentage to steal calculator should be
		const precision = 0.000001;
		
		let server_target_security = ns.getServerSecurityLevel(array_of_rooted_servers_targetted[array_of_rooted_servers_targetted.length - 1]);
//		weaken grow, hack
//		run loop if conf[0] is "true"
//		NEED TO MAKE THE FOR LOOP ASYNC SUCH THAT EACH TIME IT LOOPS IS RAN REGARDLESS OF THE PREVIOUS LOOP WORKED, EG IN PARALLEL. NEED TO UTILISE TOTAL_FREE_RAM OF NETWORK
		while (true) {
				server_buy_or_upgrade(ns, cash_per_ram, server_amount_max, server_ram_min, server_ram_max, host, name);
				array_of_servers = return_array_of_servers(ns, host);
				open_ports_nuke(ns, array_of_exploits, host, array_of_unrooted_servers);
				array_of_rooted_servers_used = return_array_of_rooted_servers(ns, array_of_servers);
				array_of_rooted_servers_targetted = return_array_of_rooted_servers(ns, array_of_servers);
				sort_by_server_ram(ns, array_of_rooted_servers_used);
				sort_by_server_scores(ns, array_of_rooted_servers_targetted);
				copy_files_to_rooted_servers(ns, scripts, host, array_of_rooted_servers_used);
//				set server_used as the rooted server with the highest ram that isn't being used yet
				const server_used = array_of_rooted_servers_used[array_of_rooted_servers_used.length - 1];
//				set server_target as the rooted server with the best score that isn't being targetted yet
				const server_target = array_of_rooted_servers_targetted[array_of_rooted_servers_targetted.length - 1];
//				parameters = ns, security_decrease_weaken, security_increase_grow, security_increase_hack, cash_percentage_increase_base_grow, cash_percentage_increase_max_grow, multipliers, array_of_scripts, ram_script_weaken, ram_script_grow, ram_script_hack, server_used, server_target, server_target_security, padding, precision
				const server_target_security_current = await weaken_grow_hack(ns, security_decrease_weaken, security_increase_grow, security_increase_hack, cash_percentage_increase_base_grow, cash_percentage_increase_max_grow, multipliers, scripts, ram_script_weaken, ram_script_grow, ram_script_hack, server_used, server_target, server_target_security, padding, precision);
				server_target_security = server_target_security_current;
				await ns.sleep(1);
		}
		await ns.sleep(1);
	}
};