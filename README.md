# Bitburner Scripts
My scripts for the game [Bitburner](https://danielyxie.github.io/bitburner/ "Link to the Bitburner game"), evidently.

# Purpose
TODO.

# Instructions

(These will definitely change later on)

For version 1.4
You need to have "weaken.js", "grow.js", "hack.js", "hack_delayed.js" and a "conf.txt" in the computer that you're running the "main.js" from. These can be found in the in the "helpers" directories (the first three are in the "1.0/helpers" directory, "hack_delayed.js" is in the "1.3/helpers" directory and "conf.txt" is in the "1.4/helpers" directory). You also need at least 17.05Gb on the "home" server from where the "main.js" should be run.

Once you have the files, the "main.js" is run by entering:
```
run <script>
```
or if you want to choose the parameters without needing to change them on-the-fly later on:
```
run <script> <percentage> <true/false> <target>
```
where <script> is the filename you saved the "main.js" as in your game. <percentage> is a decimal in the range 0 to 1 and is used to determine roughly what percentage of the target server's money will be taken (not accurate, will always be less than the value you used because of how the script works). <true/false> can be true or anything else (if empty, defaults to false, if anything other than true, then it's also false). If you set it as true, servers will automatically be bought or replaced. <target> is the hostname of the server you want to hack but specifying it is optional depending on if you want to manually specify a target or if you want the script to chose an optimal target automatically.

If you ran the "main.js" without any arguments, it will instead read the "conf.txt" file to set its settings. The format of the file is :
```
<percentage> <true/false> <target>
```
And it works in much the same way as passing arguments to the main script except that the configuration file can be edited to change the settings of the main script on-the-fly.


For version 2.0
You need to have "weaken.js", "grow.js", "hack.js", "lib.js" (the first three are in "2.0/helpers" and "lib.js" is in "2.0/lib") in computer that you're running the "main.js" from.

Once you have the files, the "main.js" is run by entering:
```
run <script>