export const main = async function (ns) {
	for (let indices_0 = 0; indices_0 < ns.args.length; ++indices_0) {
//		while (true) {
			const cash_max = ns.getServerMaxMoney(ns.args[indices_0]);
			const cash_current = ns.getServerMoneyAvailable(ns.args[indices_0]);
			const security_min = ns.getServerMinSecurityLevel(ns.args[indices_0]);
			const security_current = Math.round(ns.getServerSecurityLevel(ns.args[indices_0]));
			const ram = ns.getServerRam(ns.args[indices_0]);
			const ram_total = ram[0];
			const ram_used = ram[1];
			const ram_free = ram[0] - ram[1];
			const data = [
//				comment out unneeded info
				"Name:\t\t\t" + ns.args[indices_0],
				"Root access:\t\t" + ns.hasRootAccess(ns.args[indices_0]),
				"Maximum cash ($):\t" + cash_max,
				"Current cash ($):\t" + cash_current.toFixed(2),
				"Current cash (%):\t" + (cash_current * 100 / cash_max).toFixed(2),
				"Growth:\t\t" + ns.getServerGrowth(ns.args[indices_0]),
				"Minimum security:\t" + security_min,
				"Current security:\t" + security_current,
				"Current security (x):\t" + (security_current / security_min).toFixed(2),
				"Hacking level needed:\t" + ns.getServerRequiredHackingLevel(ns.args[indices_0]),
				"Ports needed for root:\t" + ns.getServerNumPortsRequired(ns.args[indices_0]),
				"RAM total (Gb):\t" + ram_total,
				"RAM used (Gb):\t\t" + ram_used.toFixed(2),
				"RAM used (%):\t\t" + (ram_used * 100 / ram_total).toFixed(2),
				"RAM free (Gb):\t\t" + ram_free.toFixed(2),
				"RAM free (%):\t\t" + (ram_free * 100 / ram_total).toFixed(2),
				"\n"
			];
			for (let indices_0 = 0; indices_0 < data.length; ++indices_0) {
				ns.tprint(data[indices_0]);
			}
//			ns.sleep(1000);
//		}	
	}
};