// returns an array of all servers in the game
const return_array_of_servers = function (ns, host) {
	const array_of_servers = [host];
	for (var indices_0 = 0; indices_0 < array_of_servers.length; indices_0++) {
		const array_of_scan_results = ns.scan(array_of_servers[indices_0]);
		for (var indices_1 = 0; indices_1 < array_of_scan_results.length; indices_1++) {
			if (array_of_servers.indexOf(array_of_scan_results[indices_1]) === -1) {
				array_of_servers.push(array_of_scan_results[indices_1]);
			}
		}
	}
	return array_of_servers;
};

// kills all running scripts in the network
const kill_all = function (ns, host, array_of_servers) {
	// exclude host for now so that this script wont be killed
	array_of_servers.splice(0, 1);
	
	for (var indices_0 = 0; indices_0 < array_of_servers.length; indices_0++) {
	const server = array_of_servers[indices_0];
	const scripts = ns.ps(server);
		// kill each script in the server
		for (var indices_1 = 0; indices_1 < scripts.length; indices_1++) {
			ns.scriptKill(scripts[indices_1].filename, server);
		}
    }
	
	// kill scripts in host
	let scripts_host = ns.ps(host);
	for (var indices_2 = 0; indices_2 < scripts_host.length; indices_2++) {
		// ensure current this script is killed last
		if (scripts_host[indices_2].filename != ns.getScriptName()) {
			ns.scriptKill(scripts_host[indices_2].filename, host);
		}
    }
}

export async function main(ns) {
	let host = ns.getHostname();
	let array_of_servers = return_array_of_servers(ns, host);
	
	kill_all(ns, host, array_of_servers);
}